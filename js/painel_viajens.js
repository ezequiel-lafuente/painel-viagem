var url = 'http://' + QLIK_HOST + ':' + QLIK_PORT + '/resources/assets/external/requirejs/require.js';
var config = {
    host: "qliksense-qap02.brazilsouth.cloudapp.azure.com",
    prefix: "/",
    port: "80",
    isSecure: false
};

require.config({
    baseUrl: (config.isSecure ? "https://" : "http://" ) + config.host + (config.port ? ":" + config.port : "") + config.prefix + "resources",
    paths: {handlebars: "//cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min"} // NecessÃ¡rio para criar o cubo.
});

require(["js/qlik"], function (qlik) {

    qlik.setOnError(function (error) {
        console.log(error.message);
    });

    app = qlik.openApp(QLIK_APP_ID, config);
    carregarTela();
});

function carregarTela() {
    app.visualization.get(QS_FILTRO_MAT_1).then(function (vis) {vis.show("QV00");});
    app.visualization.get(QS_FILTRO_MAT_2).then(function (vis) {vis.show("QV01");});


}