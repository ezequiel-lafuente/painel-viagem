$(document).ready(function () {

    $('.tab-analise-viajem').css({'font-weight':'bold', 'color' : 'black','background':'#FAFAFA'});
    $('.tab-analise-compra').css({'font-weight':'normal', 'color' : '#797979'});
    $('.tab-analise-agenciamento').css({'font-weight':'normal', 'color' : '#797979'});

    $(document).on('click', '.viajens', function(){
        $(".viajens").css('background','#357FBE');
        $(".font-viajens").css('color' , '#FAFAFA');
        $(".diarias , .passagens").css('background','#FAFAFA');
        $(".font-diarias , .font-passagens").css('color' , '#357FBE');
        $(".tab-viajem").show();
        $(".tab-diaria , .tab-passagem").hide();
    });

    $(document).on('click', '.diarias', function(){
        $(".diarias").css('background','#357FBE');
        $(".font-diarias").css('color' , '#FAFAFA');
        $(".passagens , .viajens").css('background','#FAFAFA');
        $(".font-viajens , .font-passagens").css('color' , '#357FBE');
        $(".tab-diaria").show();
        $(".tab-viajem , .tab-passagem").hide();
    });

    $(document).on('click', '.passagens', function(){
        $(".passagens").css('background','#357FBE');
        $(".font-passagens").css('color' , '#FAFAFA');
        $(".diarias , .viajens").css('background','#FAFAFA');
        $(".font-diarias , .font-viajens").css('color' , '#357FBE');
        $(".tab-passagem").show();
        $(".tab-viajem , .tab-diaria").hide();
    });

//////////////////////////////
    $(document).on('click', '.tab-analise-viajem', function(){
        $(".tab-analise-viajem").css({'font-weight':'bold','background':'#FAFAFA','color':'black'});
        $(".tab-analise-compra , .tab-analise-agenciamento").css({'font-weight':'normal','background':'#ededed','color':'#797979'});
    });

    $(document).on('click', '.tab-analise-agenciamento', function(){
        $(".tab-analise-agenciamento").css({'font-weight':'bold','background':'#FAFAFA','color':'black'});
        $(".tab-analise-compra , .tab-analise-viajem").css({'font-weight':'normal','background':'#ededed','color':'#797979'});
    });

    $(document).on('click', '.tab-analise-compra', function(){
        $(".tab-analise-compra").css({'font-weight':'bold','background':'#FAFAFA','color':'black'});
        $(".tab-analise-viajem , .tab-analise-agenciamento").css({'font-weight':'normal','background':'#ededed','color':'#797979'});
    });

});